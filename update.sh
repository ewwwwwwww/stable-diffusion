#!/bin/bash

podman kill stable-diffusion-1 || true
cd stable-diffusion-1
git stash
git pull
git stash pop
cd ..
