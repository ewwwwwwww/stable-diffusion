#!/bin/bash

FILE=./stable-diffusion-webui

echo "Downloading stable diffusion webui..."

if [ ! -d "$FILE" ]; then
  git clone https://github.com/AUTOMATIC1111/stable-diffusion-webui
fi

MODEL=./stable-diffusion-webui/models/Stable-diffusion/model.ckpt

if [ ! -f "$MODEL" ]; then
  mv ./model.ckpt $MODEL
fi

sed -i 's/numpy==.*/numpy==1.21.6/g' ./stable-diffusion-webui/requirements_versions.txt
sed -i 's/fairscale==.*/fairscale==0.4.6/g' ./stable-diffusion-webui/requirements_versions.txt
sed -i "s/IS_HIGH_VERSION = .*/IS_HIGH_VERSION = True/g" ./stable-diffusion-webui/repositories/CodeFormer/facelib/detection/yolov5face/face_detector.py

podman run -it --rm --name "stable-diffusion-1" \
  --network=host --device=/dev/kfd --device=/dev/dri --group-add video \
  --cap-add=SYS_PTRACE --security-opt seccomp=unconfined \
  -v $PWD/stable-diffusion-webui:/data \
  "rocm/pytorch:latest" \
  sh -c "cd /data && python3 launch.py"
