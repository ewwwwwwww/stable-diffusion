# Instructions

1. download stable diffusion

go here: https://rentry.org/sdmodels#stable-diffusion-v15-81761151-a9263745

download and rename stable diffusion file as model.ckpt (next to start.sh file)

2. install deps

needs podman + git

3. run

`sh start.sh`
